#ifndef FUNCTIONS_LANG
#define FUNCTIONS_LANG


#ifdef FRENCH

#define _sleep attendre

#define _stopProgram arreterProgramme

#endif


#ifdef ENGLISH

#define _sleep sleep

#define _stopProgram stopProgram

#endif


#endif
