#include "functions.h"
#include <Arduino.h>

void _sleep(unsigned long ms){
    delay(ms);
}

void _stopProgram(){
    for(int i=0; i<=13; ++i){   
        digitalWrite(i, LOW);
        pinMode(i, INPUT);
    }

    while (true){}
}