#ifndef ROSA_H
#define ROSA_H

#include "ROSA_lang.h"

#include <NewPing.h>

class ROSA{
    private:
        int rSpeed;
        int lSpeed;

        int mR1;
        int mR2;
        int mL1;
        int mL2;

        int trigPin;
        int echoPin;
        
        int maxDist;
        int minDist;

        bool obst;

        NewPing distanceCM;


    
    public:

        ROSA();
        ROSA(int mR1, int mR2, int mL1, int mL2, int trigPin, int echoPin);
        ROSA(int mR1, int mR2, int mL1, int mL2, int trigPin, int echoPin, int rSpeed, int lSpeed, int maxDist, int minDist);

        void _moveForward();
        void _moveForward(int);
        void _moveBack();
        void _moveBack(int);
        void _turnRight();
        void _turnRight(int);
        void _turnLeft();
        void _turnLeft(int);
        void _rotateRight();
        void _rotateRight(int);
        void _rotateLeft();
        void _rotateLeft(int);
        void _stop();

        unsigned int _measureDistance();
        unsigned int _measureDistance(int);

        void _setRSpeed(int);
        void _setLSpeed(int);
        void _setMaxDist(int);
        void _setMinDist(int);


        bool _obstacle();

};


#endif