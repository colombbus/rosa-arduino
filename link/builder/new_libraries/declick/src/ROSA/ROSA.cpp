#include <NewPing.h>
#include <Arduino.h>
#include "ROSA.h"




ROSA::ROSA()
    : mR1 (5), mR2 (6), mL1 (9), mL2 (10), trigPin (12), echoPin (11), rSpeed (250), lSpeed (250), maxDist (15), minDist(0), obst (false), distanceCM(trigPin, echoPin)
{
    //right motor
    pinMode(mR1, OUTPUT);
    pinMode(mR2, OUTPUT);

    //left motor
    pinMode(mL1, OUTPUT);
    pinMode(mL2, OUTPUT);

    _stop();

}

ROSA::ROSA(int mR1, int mR2, int mL1, int mL2, int trigPin, int echoPin)
    : mR1 (mR1), mR2 (mR2), mL1 (mL1), mL2 (mL2), trigPin (trigPin), echoPin (echoPin), rSpeed (150), lSpeed (150), maxDist (50), minDist(0), obst (false), distanceCM(trigPin, echoPin)
{
    ROSA();

}

ROSA::ROSA(int mR1, int mR2, int mL1, int mL2, int trigPin, int echoPin, int rSpeed, int lSpeed, int maxDist, int minDist)
    : mR1 (mR1), mR2 (mR2), mL1 (mL1), mL2 (mL2), trigPin (trigPin), echoPin (echoPin), rSpeed (rSpeed), lSpeed (lSpeed), maxDist (maxDist), minDist(minDist), obst (false), distanceCM(trigPin, echoPin)
{
    ROSA();

}

void ROSA::_moveForward(){
    //R motor
    analogWrite(mR1, rSpeed);
    analogWrite(mR2, LOW);

    //L motor
    analogWrite(mL1, lSpeed);
    analogWrite(mL2, LOW);

}

void ROSA::_moveBack(){
    //R motor
    analogWrite(mR1, LOW);
    analogWrite(mR2, rSpeed);

    //L motor
    analogWrite(mL1, LOW);
    analogWrite(mL2, lSpeed);

}

void ROSA::_turnLeft(){
    //R motor
    analogWrite(mR1, rSpeed);
    analogWrite(mR2, LOW);

    //L motor
    analogWrite(mL1, LOW);
    analogWrite(mL2, LOW);

}

void ROSA::_turnRight(){
    //R motor
    analogWrite(mR1, LOW);
    analogWrite(mR2, LOW);

    //L motor
    analogWrite(mL1, lSpeed);
    analogWrite(mL2, LOW);

}

void ROSA::_rotateLeft(){
    //R motor
    analogWrite(mR1, rSpeed);
    analogWrite(mR2, LOW);

    //L motor
    analogWrite(mL1, LOW);
    analogWrite(mL2, lSpeed);

}

void ROSA::_rotateRight(){
    //R motor
    analogWrite(mR1, LOW);
    analogWrite(mR2, rSpeed);

    //L motor
    analogWrite(mL1, lSpeed);
    analogWrite(mL2, LOW);

}

void ROSA::_stop(){
    //R motor
    analogWrite(mR1, LOW);
    analogWrite(mR2, LOW);

    //L motor
    analogWrite(mL1, LOW);
    analogWrite(mL2, LOW);

}

unsigned int ROSA::_measureDistance(){
    unsigned int cm = distanceCM.ping_cm();

    if (cm > minDist && cm < maxDist){
        obst = true;
    }
    else{
        obst = false;
    }

    return cm;
}

void ROSA::_moveForward(int t){
    _moveForward();

    delay(t);
    _stop();

}

void ROSA::_moveBack(int t){
    _moveBack();

    delay(t);
    _stop();

}

void ROSA::_turnLeft(int t){
    _turnLeft();

    delay(t);
    _stop();

}

void ROSA::_turnRight(int t){
    _turnRight();

    delay(t);
    _stop();

}

void ROSA::_rotateLeft(int t){
    _rotateLeft();

    delay(t);
    _stop();

}

void ROSA::_rotateRight(int t){
    _rotateRight();

    delay(t);
    _stop();

}

unsigned int ROSA::_measureDistance(int t){
    delay(t);
    
    return _measureDistance();
}

void ROSA::_setRSpeed(int v){
    rSpeed = v;
}

void ROSA::_setLSpeed(int v){
    lSpeed = v;
}

void ROSA::_setMaxDist(int d){
    maxDist = d;
}

void ROSA::_setMinDist(int d){
    minDist = d;
}

bool ROSA::_obstacle(){
    return obst;
}