#ifndef LED_H
#define LED_H

#include "LED_lang.h"

class LED{
    private:
        int pin;

    public:
        LED();
        LED(int pin);

        void _on();
        void _on(int t);
        void _off();
};



#endif