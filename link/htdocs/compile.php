<?php

/**
 * compile code through arduino builder
 */

include '../config.php';

// delete $dir & its content
function delTree($dir)
{
    $files = array_diff(scandir($dir), array('.', '..'));

    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }

    return rmdir($dir);
}

// get file content
$data = json_decode(file_get_contents("php://input"));

// dir & files creation
do {
    $dir = uniqid();
} while (is_dir("$pathToBuilder/tmp/$dir"));

if (!(mkdir("$pathToBuilder/tmp/$dir", 0777, true) &&
    mkdir("$pathToBuilder/tmp/$dir/file", 0777, true) &&
    mkdir("$pathToBuilder/tmp/$dir/bin", 0777, true) &&
    ($file = fopen("$pathToBuilder/tmp/$dir/file/file.ino", 'w+')) &&
    fwrite($file, $data->data) !== false &&
    fclose($file))) {
    header("HTTP/1.1 500 Internal Server Error");
    return;
}

// flux
$descriptorspec = array(
    0 => array("pipe", "r"), // stdin
    1 => array("pipe", "w"), // stdout
    2 => array("pipe", "w"), // stderr
);

// launch compilation
$process = proc_open("./arduino-builder -hardware hardware -tools hardware/tools -tools tools-builder -libraries libraries -libraries new_libraries -fqbn $data->board -build-path tmp/$dir/bin -prefs=lang=$data->lang -compile tmp/$dir/file", $descriptorspec, $pipes, $pathToBuilder);

$stdout = stream_get_contents($pipes[1]);
fclose($pipes[1]);

$stderr = stream_get_contents($pipes[2]);
fclose($pipes[2]);

proc_close($process);

// get all compilation messages & files if compilation succeeded
if ($stderr) {
    $status = false;
    $hex = "";
    $elf = "";
} else {
    $status = true;
    $hex = file_get_contents("$pathToBuilder/tmp/$dir/bin/file.ino.hex", 'r');
    $elf = file_get_contents("$pathToBuilder/tmp/$dir/bin/file.ino.elf", 'r');
}

// temp dir & files deletion
delTree("$pathToBuilder/tmp/$dir");

$result = ["status" => $status, "stdout" => $stdout, "stderr" => str_replace("$pathToBuilder", "", $stderr), "hex" => base64_encode($hex), "elf" => base64_encode($elf), "elf_url" => "/v1/compile/file.elf", "hex_url" => "/v1/compile/file.hex"];

echo json_encode($result);
