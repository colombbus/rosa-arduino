<?php

/**
 * send cleaned list card
 */
include '../config.php';

if (($boards = json_decode(file_get_contents("boards.json", 'r'), true)) !== null) {
    echo json_encode(array_map(
        function ($e) {
            if (isset($e["name"], $e["pid"], $e["vid"], $e["fqbn"])) {
                return ["name" => $e["name"], "pid" => $e["pid"], "vid" => $e["vid"], "fqbn" => $e["fqbn"]];
            }
        }, $boards));
} else {
    header("HTTP/1.1 500 Internal Server Error");
    // header("HTTP/1.1 500 Internal Server Error", false, 500);
}
