<?php

/**
 * send signed commmandline to upload hex
 */
include '../config.php';

// get input, arduino cards, private-key
$data = json_decode(file_get_contents("php://input"), true);

$boards = json_decode(file_get_contents("boards.json", 'r'), true);

$key = file_get_contents("../priv.pem");

if (isset($boards[$_GET["fqbn"]])) {
    $board = $boards[$_GET["fqbn"]];

    if (preg_match("/.*\:avr\:.*/", $board["fqbn"])) { //commande pour architecture avr

        $mcu = $board["build.mcu"];
        $protocol = $board["upload.protocol"];
        $speed = $board["upload.speed"];

        $cmd = '"{runtime.tools.avrdude.path}/bin/avrdude" "-C{runtime.tools.avrdude.path}/etc/avrdude.conf" -v -p' . $mcu . ' -c' . $protocol . ' "-P{serial.port}" -b' . $speed . ' -D "-Uflash:w:{build.path}/{build.project_name}.hex:i"';
    } else {
        header("HTTP/1.1 501 Not Implemented");
        return;
    }

    // SSL command signature
    openssl_sign($cmd, $signature, $key, "SHA256");

    $res = ["commandline" => $cmd,
        "signature" => bin2hex($signature),
        "options" => []];

    echo json_encode($res);
} else {
    header("HTTP/1.1 404 Not Found");
    // header("HTTP/1.1 404 Not Found", true, 404);
}
