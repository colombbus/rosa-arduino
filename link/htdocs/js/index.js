const codeArea = 'codeArea'; // élément qui contient le code
const compileButton = 'compileButton'; // bouton pour compiler
const uploadButton = 'uploadButton'; //bouton pour compiler et téléverser
const selectBoard = 'selectBoard'; //liste déroulante pour sélectionner une carte connectée
const selectOtherBoard = 'selectOtherBoard'; //liste déroulante pour sélectionner une carte non connectée
const compilationLogArea = 'logArea'; //élément pour afficher les messages de compilation
const uploadLogArea = 'logArea'; //élément pour afficher les messages de téléversement
const agentStatus = 'agentStatus';
const channelStatus = 'channelStatus';


const compileURL = 'compile.php'; //URL pour compiler
const boardsURL = 'boards.php'; //URL pour récupérer la liste des cartes supportées
const daemon = new ArdCreAgtDaemon('/builder/');

var boardsList = {};





/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////
//récupération du code entré dans l'éditeur
function getFiles() {
  return document.getElementById(codeArea).value;

}




function loadFile() {
  fr = new FileReader();
  fr.onload = function () { document.getElementById(codeArea).value = fr.result; }

  fr.readAsText(document.getElementById('fileInput').files[0]);

}


/*
envoie le code au serveur pour qu'il le compile

arguments :
- bool upload : si true lance le téléversement du code sur la carte après la compilation

constantes globales :
- compilationLogArea
- codeArea
- selectBoard
- selectOtherBoard
*/
function compile(upload) {

  var data = getFiles();

  if (document.getElementById(selectBoard).value != "other") {
    var board = JSON.parse(document.getElementById(selectBoard).value).board;
  }
  else {
    var board = document.getElementById(selectOtherBoard).value;
  }

  var xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {

      var res = JSON.parse(this.responseText);

      var compilationLog = document.getElementById(compilationLogArea);

      if (res["status"]) {//compilation réussie
        compilationLog.innerHTML = res["stdout"].replace(/\n/g, "<br>").replace(/ /g, "&nbsp;");

        if (upload) uploadSketch(res);
      }
      else {//compilation échouée
        compilationLog.innerHTML = res["stderr"].replace(/\n/g, "<br>").replace(/ /g, "&nbsp;");
      }


    }
  };

  xhr.open('POST', compileURL, true);
  xhr.send(JSON.stringify({ "data": data, "board": board, "lang": "FRENCH" }));

}



/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////


/*
récupère la liste des cartes supportées et les met dans la liste déroulante selectOtherBoard

constantes globales :
- selectOtherBoard
- boardsURL

variables globales :
- boardsList (contiendra la liste des cartes)

*/
function getBoardsList() {
  var xhr = new XMLHttpRequest();

  xhr.onloadend = function () {
    boardsList = JSON.parse(this.responseText);

    var select = document.getElementById(selectOtherBoard);
    select.innerHTML = "";

    for (var board in boardsList) {
      if (boardsList[board] != null)
        select.innerHTML += "<option value='" + board + "'>" + boardsList[board].name + "</option>";
    }

    setupArdCreAgtDaemon();

  };


  xhr.open("GET", boardsURL);
  xhr.send();
}




/*
retourne les informations d'une carte à partir de son pid et de son vid
retourne false si pid et vid ne correspondent à aucune carte

arguments :
- string pid : pid d'une carte
- string vid : pid d'une carte

variables globales :
- boardsList (doit contenir la liste des cartes)

*/
function getBoardInfo(pid, vid) {
  var i;

  for (var board in boardsList) {

    if (boardsList[board] == null) continue;

    for (i in boardsList[board].vid) {

      if (boardsList[board].vid[i] == vid && boardsList[board].pid[i] == pid) {
        return boardsList[board];
      }
    }
  }

  return false;
}





function setupArdCreAgtDaemon() {

  /*lors d'une MàJ de l'état de l'agent affiche son état dans agentStatus

  constantes globales :
   - agentStatus
  */
  daemon.agentFound.subscribe(status => {
    document.getElementById(agentStatus).innerHTML = status;
  });


  /*lors d'une MàJ de l'état du canal affiche son état dans channelStatus
  
  constantes globales :
  - channelStatus
  */
  daemon.channelOpenStatus.subscribe(status => {
    document.getElementById(channelStatus).innerHTML = status;
  });


  /*lors d'une erreur, affiche l'erreur dans la console
  */
  daemon.error.subscribe(err => {
    console.log(err);
  });


  /*lors d'une MàJ des cartes connectées, les ajoute à la liste déroulante selectBoard
  
  constantes globales :
  - selectBoard
  */
  daemon.devicesList.subscribe(function ({ serial, network }) {

    document.getElementById("serialDevices").innerHTML = "";
    document.getElementById("networkDevices").innerHTML = "";

    for (board of serial) {
      for (id in board) {
        document.getElementById("serialDevices").innerHTML += id + " : " + board[id] + "<br/>";
      }
      document.getElementById("serialDevices").innerHTML += "<br/>"
    }
    for (board of network) {
      for (id in board) {
        document.getElementById("networkDevices").innerHTML += id + " : " + board[id] + "<br/>";
      }
      document.getElementById("networkDevices").innerHTML += "<br/>"
    }


    var select = document.getElementById(selectBoard);
    select.innerHTML = "";


    //ajout des cartes connectées à la liste déroulante selectBoard
    for (var board in serial) {
      var boardInfo = getBoardInfo(serial[board].ProductID, serial[board].VendorID);
      select.innerHTML += "<option value='" + JSON.stringify({ board: boardInfo.fqbn, port: serial[board].Name }) + "' >" + boardInfo.name + " sur " + serial[board].Name + "</option>";

    }


    select.innerHTML += "<option value='other'>autre</option>";

    updateSelectBoard();

  });




  // Upload progress
  daemon.uploading.subscribe(upload => {
    if ('msg' in upload) document.getElementById(uploadLogArea).innerHTML += upload["msg"] + "<br/>";
  });

}


/*
téléverse hex sur la carte sélectionnée par selectBoard

arguments :
- string hex : résultat de la requête de compilation par le serveur

constantes globales :
- selectBoard

*/
function uploadSketch(hex) {

  var target = JSON.parse(document.getElementById(selectBoard).value);

  var sketchName = "test";

  var compilationResult = hex;

  var verbose = true;

  daemon.uploadSerial(target, sketchName, compilationResult, verbose);


}


/*affiche la liste déroulante selectOtherBoard et désactive le bouton upload si "other" est sélectionné sur selectBoard
sinon cache la liste déroulante et active le bouton

constantes globales :
- selectBoard
- selectOtherBoard
- uploadButton
*/
function updateSelectBoard() {

  if (document.getElementById(selectBoard).value == "other") {
    document.getElementById(selectOtherBoard).style = "";
    document.getElementById(uploadButton).disabled = true;
  }
  else {
    document.getElementById(selectOtherBoard).style = "display:none;";
    document.getElementById(uploadButton).disabled = false;
  }

}


// événements

document.getElementById(compileButton).onclick = function () { compile(false); };
document.getElementById(uploadButton).onclick = function () { compile(true); };
document.getElementById(selectBoard).onchange = updateSelectBoard;


getBoardsList();

