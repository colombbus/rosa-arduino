<?php

/*
 *generate public and private keys
 */

ini_set('display_errors', 'On');
error_reporting(E_ALL);


$a = openssl_pkey_new(array(
    "private_key_bits" => 2048,
    "private_key_type" => OPENSSL_KEYTYPE_RSA,
));

openssl_pkey_export($a, $privKey);


$file = fopen("priv.pem", 'w+');

fwrite($file, $privKey);
fclose($file);


$pubKey = openssl_pkey_get_details($a);
$pubKey = $pubKey["key"];

$file = fopen("pub.pem", 'w+');

fwrite($file, $pubKey);
fclose($file);
