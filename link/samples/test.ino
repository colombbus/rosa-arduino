#include <declick.h>

ROSA rosa;
int sens;

auto toupie()
{
    if(sens == 1){
        rosa.rotationDroite(1000);
    }
    else{
        rosa.rotationGauche(1000);
    }
    return 0;
}

void setup(){
    sens = 0;
}

void loop(){
    toupie();
    rosa.avancer();
    
    rosa.mesurerDistance();
    
    if(rosa.obstacle()){
        toupie();
        
        switch(sens){
            case 1:
                sens = 0;
                break;
            case 0:
                sens = 1;
                break;
        }
    }
}
