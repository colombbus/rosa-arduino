<?php

/*
 * create file containing informations about boards
 * */


include 'config.php';

$boards = [];

$archList = ["avr"];

foreach ($archList as $arch){

    $curl = curl_init("https://raw.githubusercontent.com/arduino/ArduinoCore-$arch/master/boards.txt");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $return = curl_exec($curl);
    curl_close($curl);

    $lines = preg_split('/\n/', $return);

    $boardIds = array_values(preg_filter("/^(.*)\.vid\.0\=.*$/", "$1", $lines));

    foreach ($boardIds as $boardId){

        if ($name = array_values(preg_filter("/^$boardId\.name\=(.*)$/", "$1", $lines)) &&
        $vid = array_values(preg_filter("/^$boardId\.vid\.\d*\=(.*)$/", "$1", $lines)) &&
        $pid = array_values(preg_filter("/^$boardId\.pid\.\d*\=(.*)$/", "$1", $lines)) &&
        $mcu = array_values(preg_filter("/^$boardId\.build.mcu\=(.*)$/", "$1", $lines)) &&
        $protocol = array_values(preg_filter("/^$boardId\.upload.protocol=(.*)$/", "$1", $lines)) &&
        $speed = array_values(preg_filter("/^$boardId\.upload.speed=(.*)$/", "$1", $lines))){

            $boards["arduino:$arch:$boardId"] = [
                "fqbn" => "arduino:$arch:$boardId",
                "name" => array_values(preg_filter("/^$boardId\.name\=(.*)$/", "$1", $lines))[0],
                "vid" => array_values(preg_filter("/^$boardId\.vid\.\d*\=(.*)$/", "$1", $lines)),
                "pid" => array_values(preg_filter("/^$boardId\.pid\.\d*\=(.*)$/", "$1", $lines)),
                "build.mcu" => array_values(preg_filter("/^$boardId\.build.mcu\=(.*)$/", "$1", $lines))[0],
                "upload.protocol" => array_values(preg_filter("/^$boardId\.upload.protocol=(.*)$/", "$1", $lines))[0],
                "upload.speed" => array_values(preg_filter("/^$boardId\.upload.speed=(.*)$/", "$1", $lines))[0],
            ];
        }

    }

}

$file = fopen("boards.json", 'w+');

fwrite($file, json_encode($boards));
fclose($file);
