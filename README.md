# Arduino & Rosa robot

## folder & project tree

- `builder/` contains files required to compile on server
- `librairies/` arduino library corresponding to declick objects ROSA & LED
- `link/` contains a sandbox/test page to compile on server

## create Agent source code

- install first : https://github.com/arduino/arduino-create-agent

## Privacy policy createAgent

https://www.arduino.cc/PrivacyPolicy/en/

## Terms of Service for createAgent

https://www.arduino.cc/en/Main/TermsOfService

## update configuration

- install CreateAgent from https://create.arduino.cc/getting-started/plugin?page=2
- update configuration by adding a custom config like `config.declick.ini`
