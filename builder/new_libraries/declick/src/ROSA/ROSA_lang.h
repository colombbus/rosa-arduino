#ifndef ROSA_LANG
#define ROSA_LANG


#ifdef FRENCH

#define ROSA ROSA

#define _moveForward avancer
#define _moveBack reculer
#define _turnRight tournerDroite
#define _turnLeft tournerGauche
#define _rotateRight rotationDroite
#define _rotateLeft rotationGauche
#define _stop stop
#define _measureDistance mesurerDistance
#define _setRSpeed modifierVitesseDroite
#define _setLSpeed modifierVitesseGauche
#define _obstacle obstacle
#define _setMaxDist modifierDistanceMax
#define _setMinDist modifierDistanceMin
#define _backLeft reculerGauche
#define _backRight reculerDroite

#endif


#ifdef ENGLISH

#define ROSA ROSA

#define _moveForward moveForward
#define _moveBack moveBack
#define _turnRight turnRight
#define _turnLeft turnLeft
#define _rotateRight rotateRight
#define _rotateLeft rotateLeft
#define _stop stop
#define _measureDistance measureDistance
#define _setRSpeed setRSpeed
#define _setLSpeed setLSpeed
#define _obstacle obstacle
#define _setMaxDist setMaxDist
#define _setMinDist setMinDist
#define _backLeft backLeft
#define _backRight backRight

#endif


#endif
