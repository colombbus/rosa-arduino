#include "SerialPort.h"
#include <Arduino.h>
#include <Print.h>

SerialPort::SerialPort(){
}



size_t SerialPort::_send(const __FlashStringHelper *val){
    return Serial.println(val);
}

size_t SerialPort::_send(void){
    return Serial.println();
}

size_t SerialPort::_send(const String &val){
    return Serial.println(val);
}

size_t SerialPort::_send(const char val[]){
    return Serial.println(val);
}

size_t SerialPort::_send(char val){
    return Serial.println(val);
}

size_t SerialPort::_send(const Printable& val){
    return Serial.println(val);
}

size_t SerialPort::_send(unsigned char val){
    return Serial.println(val);
}

size_t SerialPort::_send(int val){
    return Serial.println(val);
}

size_t SerialPort::_send(unsigned int val){
    return Serial.println(val);
}

size_t SerialPort::_send(long val){
    return Serial.println(val);
}

size_t SerialPort::_send(unsigned long val){
    return Serial.println(val);
}

size_t SerialPort::_send(double val){
    return Serial.println(val);
}




String SerialPort::_receive(){
    return Serial.readStringUntil('\n');
}

String SerialPort::_receive(char terminator){
    return Serial.readStringUntil(terminator);
}

float SerialPort::_receiveFloat(){
    return Serial.parseFloat();
}

float SerialPort::_receiveFloat(enum LookaheadMode lookahead){
    return Serial.parseFloat(lookahead);
}

float SerialPort::_receiveFloat(enum LookaheadMode lookahead, char ignore){
    return Serial.parseFloat(lookahead, ignore);
}

int SerialPort::_receiveInt(){
    return Serial.parseInt();
}

int SerialPort::_receiveInt(enum LookaheadMode lookahead){
    return Serial.parseInt(lookahead);
}

int SerialPort::_receiveInt(enum LookaheadMode lookahead, char ignore){
    return Serial.parseInt(lookahead, ignore);
}



void SerialPort::_setTimeout(long time){
    return Serial.setTimeout(time);
}



bool SerialPort::_isConnected(){
    return Serial;
}



void SerialPort::_start(){
    return Serial.begin(9600);
}

void SerialPort::_start(long speed){
    return Serial.begin(speed);
}

void SerialPort::_start(long speed, unsigned char config){
    return Serial.begin(speed, config);
}



void SerialPort::_end(){
    return Serial.end();
}



void SerialPort::_clearMessageBuffer(){
    while(Serial.available()>0){
        Serial.read();
    }
}