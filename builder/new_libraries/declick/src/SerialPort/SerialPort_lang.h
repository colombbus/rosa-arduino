#ifndef SERIALPORT_LANG
#define SERIALPORT_LANG


#ifdef FRENCH

#define SerialPort PortSerie

#define _send envoyer

#define _receive recevoir
#define _receiveFloat recevoirFlottant
#define _receiveInt recevoirEntier

#define _setTimeout definirDelai

#define _isConnected estConnecte

#define _start demarrer

#define _end terminer

#define _clearMessageBuffer effacerMessages

#endif


#ifdef ENGLISH

#define SerialPort SerialPort

#define _send send

#define _receive receive
#define _receiveFloat receiveFloat
#define _receiveInt receiveInt

#define _setTimeout setTimeout

#define _isConnected isConnected

#define _start start

#define _end end

#define _clearMessageBuffer clearMessages

#endif


#endif
