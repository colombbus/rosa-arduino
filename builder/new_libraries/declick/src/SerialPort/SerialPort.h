#ifndef SERIALPORT_H
#define SERIALPORT_H

#include "SerialPort_lang.h"
#include <Arduino.h>
#include <Print.h>

class SerialPort{
    public:
        SerialPort();

        size_t _send(const __FlashStringHelper *ifsh);
        size_t _send(void);
        size_t _send(String const &s);
        size_t _send(const char c[]);
        size_t _send(char c);
        size_t _send(const Printable& x);
        size_t _send(unsigned char);
        size_t _send(int);
        size_t _send(unsigned int);
        size_t _send(long);
        size_t _send(unsigned long);
        size_t _send(double);

        String _receive();
        String _receive(char t);
        float _receiveFloat();
        float _receiveFloat(enum LookaheadMode lookahead);
        float _receiveFloat(enum LookaheadMode lookahead, char ignore);
        int _receiveInt();
        int _receiveInt(enum LookaheadMode lookahead);
        int _receiveInt(enum LookaheadMode lookahead, char ignore);

        void _setTimeout(long time);

        bool _isConnected();

        void _start();
        void _start(long speed);
        void _start(long speed, unsigned char config);

        void _end();

        void _clearMessageBuffer();

};



#endif