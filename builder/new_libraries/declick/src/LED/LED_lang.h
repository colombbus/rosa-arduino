#ifndef LED_LANG
#define LED_LANG


#ifdef FRENCH

#define LED DEL

#define _on allumer
#define _off eteindre

#endif


#ifdef ENGLISH

#define LED LED

#define _on on
#define _off off

#endif


#endif
