#include "LED.h"
#include <Arduino.h>

LED::LED()
    :pin(13)
{
    pinMode(pin, OUTPUT);

    _off();
}

LED::LED(int pin)
    :pin(pin)
{
    LED();
}

void LED::_on(){
    digitalWrite(pin, HIGH);
}

void LED::_on(int t){
    _on();

    delay(t);

    _off();
}

void LED::_off(){
    digitalWrite(pin, LOW);
}