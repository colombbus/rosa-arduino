// -------------------------------------------------
// Exemple de la librairie NewPing
// Mesure d'une distance avec le capteur ultrason
// --------------------------------------------------

#include <NewPing.h>

#define trigPin        12   // Trig du capteur branché sur la broche 12 de l'arduino
#define echoPin        11   // Echo du capteur branché sur la broche 11 de l'arduino
#define distance_max   400  // On définit la plage de mesure du capteur (entre 0-400 cm)

NewPing distance(trigPin, echoPin, distance_max);  // On initialise la fonction distance



/***********************************************************************************************************************************
La fonction setup() est appelée au démarrage du programme.
La fonction setup n'est exécutée qu'une seule fois, après chaque mise sous tension ou reset (réinitialisation) de la carte Arduino
***********************************************************************************************************************************/

void setup() {

  Serial.begin(9600);  // On régle la vitesse du moniteur série à 9600 baud pour afficher le résultat
}

// *************************************************
//      loop() = le programme principal
// *************************************************

void loop() {

  delay(250);                        // On attend 250ms entre chaque mesure (de pas descendre en dessous de 29ms)

  Serial.print("Distance: ");

  Serial.print(distance.ping_cm()); // Affichage de la distance en cm dans le moniteur série. Retourne 0 si la distance n'est pas dans la plage du capteur (0-400cm)

  Serial.println("cm");
}
